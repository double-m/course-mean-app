# My First MEAN Application

## Usage

Development

```
node_modules/nodemon/bin/nodemon.js bin/www -e js -w server
```

Production

```
npm start
```

Testing

```
npm test
```

## Prerequisites

### Database

For usage with PostgresSQL, here's how to prepare the database:

```
psql # as postgres
-- DROP DATABASE "mean-course-app"; DROP DATABASE "mean-course-app-test"; DROP USER "mean-course-user";
  CREATE USER "mean-course-user" WITH PASSWORD "mean-course-pass";
  CREATE DATABASE "mean-course-app";
  GRANT ALL PRIVILEGES ON DATABASE "mean-course-app" TO "mean-course-user";
  CREATE DATABASE "mean-course-app-test";
  GRANT ALL PRIVILEGES ON DATABASE "mean-course-app-test" TO "mean-course-user";
  \q
psql -h localhost -d mean-course-app -U mean-course-user -W # mean-course-pass
  \c mean-course-app;
  CREATE TABLE products ( id SERIAL, name VARCHAR, code VARCHAR );
  INSERT INTO products (name, code) VALUES ('Product 1', 'prod1');
  INSERT INTO products (name, code) VALUES ('Product 2', 'prod2');
  INSERT INTO products (name, code) VALUES ('Product 3', 'prod3');
  \c mean-course-app-test;
  CREATE TABLE products ( id SERIAL, name VARCHAR, code VARCHAR );
```

For MySQL:

```
mysql # as root
-- DROP USER mean_course_user@localhost; DROP DATABASE mean_course_app; DROP DATABASE mean_course_app_test;
  CREATE USER mean_course_user@localhost IDENTIFIED BY 'mean_course_pass';
  CREATE DATABASE mean_course_app;
  GRANT ALL PRIVILEGES ON mean_course_app.* TO mean_course_user@localhost;
  CREATE DATABASE mean_course_app_test;
  GRANT ALL PRIVILEGES ON mean_course_app_test.* TO mean_course_user@localhost;
mysql -u mean_course_user -pmean_course_pass
  use `mean_course_app`;
  CREATE TABLE products ( id INTEGER AUTO_INCREMENT, name VARCHAR(255), code VARCHAR(255), PRIMARY KEY(id) );
  INSERT INTO products (name, code) VALUES ('Product 1', 'prod1');
  INSERT INTO products (name, code) VALUES ('Product 2', 'prod2');
  INSERT INTO products (name, code) VALUES ('Product 3', 'prod3');
  use `mean_course_app_test`;
  CREATE TABLE products ( id INTEGER AUTO_INCREMENT, name VARCHAR(255), code VARCHAR(255), PRIMARY KEY(id) );
```