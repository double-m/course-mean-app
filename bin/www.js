var app = require('../server/app')
  , mongoose = require('mongoose')
  , port = 3000
  , mysqlTestUrl = 'mysql://mean_course_user:mean_course_pass@localhost:3306/mean_course_app_test'
  , Sequelize = require('sequelize');

var sequelize = new Sequelize(mysqlTestUrl);

app.locals.sequelize = sequelize;

sequelize
  .authenticate()
  .then(function(err) {
    console.log('Connection has been established successfully.');

    app.listen(port);
    //app.locals.Product = sequelize.define('product', sequelizeProductSchema, {timestamps: false})

    console.log('Server started on port', port);
  })
  .catch(function (err) {
    console.log('Unable to connect to the database:', err);
  });
