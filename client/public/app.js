angular.module('meanProduct', [])
  .controller('mainCtrl', function($http) {
    var main = this;

    main.editingData = {};

    main.addProduct = addProduct;
    main.updateProduct = updateProduct;
    main.deleteProduct = deleteProduct;
    main.saveProduct = saveProduct;
    main.editProduct = editProduct;
    main.abortEditingProduct = abortEditingProduct;

    getProducts();

    function editProduct(product) {
      main.editingData[product.id] = true;
    }

    function saveProduct(product) {
      updateProduct(product);
      main.editingData[product.id] = false;
    }

    function abortEditingProduct(product) {
      main.editingData[product.id] = false;
      getProducts();
    }

    function getProducts() {
      $http.get('/api/products').then(function(response) {
        main.products = response.data;
      });
    }

    function addProduct() {
      if (!main.newProductName || !main.newProductCode)
        return false;

      $http({
        method: 'POST',
        url: '/api/products',
        data: {
          product: {
            name: main.newProductName,
            code: main.newProductCode
          }
        }
      }).then(function (res) {
        if (res.status === 201) {
          getProducts();
        }
      });
    }

    function updateProduct(product) {
      $http({
        method: 'PUT',
        url: '/api/products/' + product.id,
        data: {
          product: {
            name: product.name,
            code: product.code
          }
        }
      }).then(function (res) {
        if (res.status === 200) {
          getProducts();
        }
      });
    }

    function deleteProduct(id) {
      $http({
        method: 'DELETE',
        url: '/api/products/' + id
      }).then(function (res) {
        if (res.status === 204) {
          getProducts();
        }
      });
    }
  });