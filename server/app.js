'use strict';

const express = require('express')
    , bodyParser = require('body-parser')
    , os = require('os')
    , ObjectID = require('mongodb').ObjectID
    , Sequelize = require('sequelize')
    , sequelizeProductSchema = require('./model/product');

var app = express();

app.use(bodyParser.json());

app.use('/', express.static('client/public'));

app.get('/api/products', function(request, response) {
  var Product = app.locals.sequelize.define('product', sequelizeProductSchema, {timestamps: false});
  Product.findAll().then(function (products) {
    response.send(products);
  });
});

app.post('/api/products', function(request, response) {
  var Product = app.locals.sequelize.define('product', sequelizeProductSchema, {timestamps: false})
    , receivedProduct = request.body.product
    , product = Product.build(receivedProduct);

  product
    .save(product)
    .then(function(savedProduct) {
      var outputMessage = 'inserted a new product: ' + JSON.stringify(savedProduct) + os.EOL;

      response.status(201).send(outputMessage);
      console.log(outputMessage);
    })
    .catch(function(error) {
      let outputMessage = 'not inserted product: ' + JSON.stringify(receivedProduct) + ' becuse of error: ' + error + os.EOL;

      response.status(400).send(outputMessage);
      console.log(outputMessage);

      return false;
    });
});

app.put('/api/products/:id', function(request, response, next) {
  /** @todo validate */

  var Product = app.locals.sequelize.define('product', sequelizeProductSchema, {timestamps: false})
    , id = request.params.id
    , receivedProduct = request.body.product;

  Product
    .findById(id)
    .then(function(oldProduct) {
      oldProduct.update(receivedProduct).then(function(updatedProduct) {
        let outputMessage = 'updated product "' + id + '" with data ' + JSON.stringify(updatedProduct) + os.EOL;
        response.status(200).send(outputMessage);

        console.log(outputMessage);
      });
    })
    .catch(function(error) {
      let outputMessage = 'not updated product: ' + JSON.stringify(receivedProduct) +  ' becuse of error: ' + error + os.EOL;

      response.status(400).send(outputMessage);
      console.log(outputMessage);

      return false;
    });
});

app.delete('/api/products/:id', function(request, response, next) {
  var Product = app.locals.sequelize.define('product', sequelizeProductSchema, {timestamps: false})
    , id = request.params.id;

  Product.findById(id).then(function(product) {
    product.destroy();

    let outputMessage = 'deleted product having id=' + id + '"' + os.EOL;
    response.status(204).send(outputMessage);

    console.log(outputMessage);
  });
});

module.exports = app;