const Sequelize = require('sequelize');

const sequelizeProductSchema = {
  name: { type: Sequelize.STRING, allowNull: false },
  code: { type: Sequelize.STRING, validate: { is: /^prod/ } }
};

module.exports = sequelizeProductSchema;