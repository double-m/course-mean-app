var expect = require('chai').expect
  , supertest = require('supertest')
  , app = require('../server/app')
  , mysqlTestUrl = 'mysql://mean_course_user:mean_course_pass@localhost:3306/mean_course_app_test'
  , Sequelize = require('sequelize')
  , sequelizeProductSchema = require('../server/model/product');

var sequelize = new Sequelize(mysqlTestUrl);

describe('api test', function () {
  var request
    , Product = sequelize.define('product', sequelizeProductSchema, {timestamps: false});

  before(function (done) {
    app.locals.sequelize = sequelize;

    sequelize
      .authenticate()
      .then(function(err) {
        console.log('Connection has been established successfully.');

        request = supertest(app);
        done();
      })
      .catch(function (err) {
        console.log('Unable to connect to the database:', err);
      });
  });

  beforeEach(function (done) {
    Product.truncate().then(function() {
      var prod1 = Product.build({name: 'Product 123', code: 'prod123'});
      prod1.save();
      done();
    });
  });

  describe('routing', function () {

    it('should return 200 on route /', function (done) {
      request.get('/').expect(200, [], done);
    });

    it('should return 404 on route /non_existing_route', function (done) {
      request.get('/non_existing_route').expect(404, [], done);
    });

  });

  describe('database via API', function () {

    it('should get the data of a product', function (done) {
      request.get('/api/products').expect(200)
        .end(function(err, res) {
          if(err) return done(err);

          expect(res.body).to.be.an.array;
          expect(res.body.length).to.equal(1);
          expect(res.body[0].code).to.equal('prod123');
          expect(res.body[0].name).to.equal('Product 123');

          done();
      });
    });

    it('should create a new product', function(done) {
      request
        .post('/api/products')
        .send( { product: { name: 'Product 456', code: 'prod456' } } )
        .end(function(err, res) {
          if(err) return done(err);

          expect(res.status).to.equal(201);

          done();
      });
    });

    it('should get error 400 when trying to create an invalid product - no name', function(done) {
      request
        .post('/api/products')
        .send( { product: { code: 'prod789' } } )
        .end(function(err, res) {
          if(err) return done(err);

          expect(res.status).to.equal(400);

          done();
      });
    });

    it('should get error 400 when trying to create an invalid product - invalid code', function(done) {
      request
        .post('/api/products')
        .send( { product: { name: 'Product 789', code: '789' } } )
        .end(function(err, res) {
          if(err) return done(err);

          expect(res.status).to.equal(400);

          done();
      });
    });

  });
});